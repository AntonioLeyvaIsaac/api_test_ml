exports.getCategories = function(data) {
    let categories = [];
    const {values} = data.find(item => (item.id == 'category'));
    
    values[0].path_from_root.forEach(element => {
        categories.push(element.name)
    });

    return categories;
}

exports.getItems = function(data) { 
    let items = [];
    const dataSplice = data.splice(0,4);

    dataSplice.forEach(element => {
        itemSanit = {
            id: element.id,
            title: element.title,
            price:  {
                currency: element.currency_id, 
                amount: element.price, 
                decimals: 2
            },
            address: element.address.state_name,
            picture: element.thumbnail,
            condition: getCondition(element.attributes),
            free_shipping: element.shipping.free_shipping
        }
        items.push(itemSanit);
    });

    return items;

}

getCondition = function(attributes) {
    let find = "";
    attributes.forEach(attr => {
        if(attr.id==='ITEM_CONDITION') find = (attr.value_name);
    })
    return find
}

exports.getConditionAttr = function(attributes) {
    let find = "";
    attributes.forEach(attr => {
        if(attr.id==='ITEM_CONDITION') find = (attr.value_name);
    })
    return find
}