'use strict';

const { default: axios } = require('axios');
const express = require('express');
const cors = require('cors');
const { getCategories, getItems, getConditionAttr } = require('./Utils');
const app = express();

app.use(cors());

const BODY_GENERAL = {
    name: "Isaac",
    lastname: "Antonio"
}

app.get('/api/items', (req, res) => {
    const {search} = req.query;
    
    let ResData = {
        author: BODY_GENERAL,
    }

    axios.get("https://api.mercadolibre.com/sites/MLA/search?q=" + search).then( response => {
        const categories = response.data.filters.length>0 ? getCategories(response.data.filters) : [response.data.available_filters[0].values[0].name];
        const items = getItems(response.data.results);
        ResData['categories'] = categories;
        ResData['items'] = items;
        res.status(200).send(ResData)
        
    }).catch( err => {
        console.log(err.message)
        ResData['error'] = {message: 'Ocurrió un error', detailMessage: err.message}
        res.status(400).send(ResData);
    })
})

app.get('/api/items/:id', (req, res) => {
    let ResData = {
        author: BODY_GENERAL,
    }

    const detail = axios.get('https://api.mercadolibre.com/items/' + req.params.id);
    const description = axios.get('https://api.mercadolibre.com/items/' + req.params.id + "/description");

    Promise.all([detail, description]).then(function(responses) {
        const respDetail = responses[0].data;
        const respDescription = responses[1].data;

        ResData['item'] = {
            id: respDetail.id,
            title: respDetail.title,
            price: {
                currency: respDetail.currency_id, 
                amount: respDetail.price, 
                decimals: 2,
            },
            picture: respDetail.pictures[0].url,
            condition: getConditionAttr(respDetail.attributes),
            free_shipping: respDetail.shipping.free_shipping,
            sold_quantity: respDetail.sold_quantity,
            description: respDescription.plain_text
        }
        res.status(200).send(ResData)
    }).catch(err => {
        ResData['error'] = {message: 'Ocurrió un error', detailMessage: err.message}
        res.status(400).send(ResData);
    });
})

app.listen(9090);