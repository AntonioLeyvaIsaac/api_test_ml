# API TEST MERCADO LIBRE

Este proyecto contiene la API para proveer a la aplicación de los datos requeridos

## Instalación 
Descargar el proyecto del repositorio
```sh
cd api_test_ml
npm i
npm start
```
**Note: La aplicación correrá en el puerto `9090`**

## Uso
Obtener listado de productos

`:query`: Parámetro a buscar
>http://localhost:9090/api/items?search=:query

Obtener detalle del producto

`:id`: ID del producto
>http://localhost:9090/api/items/:id

## Deployment
Se cuenta con una version de prueba desplegada en un servidor de HEROKU bajo el dominio: 
https://test-ml-api-v2.herokuapp.com

>https://test-ml-api-v2.herokuapp.com/api/items/:id

>https://test-ml-api-v2.herokuapp.com/api/items?search=:query